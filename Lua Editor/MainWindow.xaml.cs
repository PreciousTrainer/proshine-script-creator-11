﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ProShine_Script_Creator
{
    public partial class MainWindow
    {
        private readonly List<string> _names = new List<string>();
        private readonly List<string> _maps = new List<string>();
        private StringBuilder _codeBuilder;

        public MainWindow()
        {
            InitializeComponent();
            NameList.ItemsSource = _names;
            MapList.ItemsSource = _maps;
            Init();
        }

        private void Init()
        {
            OutputWin.AppendText("                   ~ProShine Script Creator~" + Environment.NewLine);
            OutputWin.AppendText("            https://proshine-bot.com/thread-2769.html" + Environment.NewLine);

            foreach (var pokeName in PokemonNames.PokemonNamesArray)
                PokeBox.Items.Add(pokeName);

            foreach (var mapName in MapNames.KantoMaps)
                KantoMapsBox.Items.Add(mapName);

            foreach (var mapName in MapNames.SeviiMaps)
                SeviiMapsBox.Items.Add(mapName);

            foreach (var mapName in MapNames.JohtoMaps)
                JohtoMapsBox.Items.Add(mapName);

            foreach (var mapName in MapNames.HoennMaps)
                HoennMapsBox.Items.Add(mapName);
                
            foreach (var mapName in MapNames.SinnohMaps)
                SinnohMapsBox.Items.Add(mapName);
                
            foreach (var mapName in MapNames.EventMaps)
                EventMapsBox.Items.Add(mapName);

            RegionBox.Items.Add("Kanto");
            RegionBox.Items.Add("Johto");
            RegionBox.Items.Add("Hoenn");
            RegionBox.Items.Add("Sinnoh");
            RegionBox.Items.Add("Sevii Islands");
            RegionBox.Items.Add("Event Maps");
        }

        private void Print(string msg)
        {
            OutputWin.AppendText("PSC: " + msg + Environment.NewLine);
            OutputWin.ScrollToEnd();
        }

        //Remove pokemon from list
        private void RemovePokemonButton_Click(object sender, RoutedEventArgs e)
        {
            if (_names.Count <= 0) return;

            if (NameList.SelectedIndex == -1) return;

            _names.RemoveAt(NameList.SelectedIndex);
            NameList.Items.Refresh();
            NameList.SelectedIndex = NameList.Items.Count - 1;
        }

        //Add pokemon to list
        private void AddPokemonButton_Click(object sender, RoutedEventArgs e)
        {
            if (_names.Contains(PokeBox.Text)) return;

            _names.Add(PokeBox.Text);
            NameList.Items.Refresh();
            NameList.SelectedIndex = NameList.Items.Count - 1;
            NameList.ScrollIntoView(NameList.SelectedItem);
        }

        //Add map name to list
        private void AddMapButton_Click(object sender, RoutedEventArgs e)
        {
            ComboBox selectedBox;

            if (RegionBox.Text == "Kanto")
                selectedBox = KantoMapsBox;
            else if (RegionBox.Text == "Sevii Islands")
                selectedBox = SeviiMapsBox;
            else if (RegionBox.Text == "Johto")
                selectedBox = JohtoMapsBox;
            else if (RegionBox.Text == "Hoenn")
                selectedBox = HoennMapsBox;
            else if (RegionBox.Text == "Sinnoh")
                selectedBox = SinnohMapsBox;
            else
                selectedBox = EventMapsBox;

            if (_maps.Contains(selectedBox.Text)) return;

            _maps.Add(selectedBox.Text);
            MapList.Items.Refresh();
            MapList.SelectedIndex = MapList.Items.Count - 1;
            MapList.ScrollIntoView(MapList.SelectedItem);
        }

        //Remove map name from list
        private void RemoveMapButton_Click(object sender, RoutedEventArgs e)
        {
            if (_maps.Count <= 0) return;

            if (MapList.SelectedIndex == -1) return;

            _maps.RemoveAt(MapList.SelectedIndex);
            MapList.Items.Refresh();
            MapList.SelectedIndex = _maps.Count - 1;
        }

        private void MoveUpButton_Click(object sender, RoutedEventArgs e)
        {
            int index = MapList.SelectedIndex;

            if (index <= 0) return;

            string tmp = _maps[index - 1];
            _maps[index - 1] = _maps[index];
            _maps[index] = tmp;

            MapList.Items.Refresh();
            MapList.SelectedIndex = index - 1;
            MapList.ScrollIntoView(MapList.SelectedItem);
        }

        private void MoveDownButton_Click(object sender, RoutedEventArgs e)
        {
            int index = MapList.SelectedIndex;

            if (index == -1 || index == _maps.Count - 1) return;

            string tmp = _maps[index + 1];
            _maps[index + 1] = _maps[index];
            _maps[index] = tmp;

            MapList.Items.Refresh();
            MapList.SelectedIndex = index + 1;
            MapList.ScrollIntoView(MapList.SelectedItem);
        }

        private void WriteCode(string code, int indent = 0)
        {
            for (var i = 0; i < indent; i++)
            {
                code = "\t" + code;
            }
            _codeBuilder.AppendLine(code);
        }

        private void OnFalseSwipeChanged(object sender, RoutedEventArgs e)
        {
            Print(FalseSwipeCheckBox.IsChecked == true ? "Using False Swipe" : "Not using Flase Swipe");
        }

        private void OnAttackWildsChanged(object sender, RoutedEventArgs e)
        {
            Print(AttackWildsCheckBox.IsChecked == true ? "Attacking normal wilds" : "Fleeing from normal wilds");
        }

        private void CreditsButton_Click(object sender, RoutedEventArgs e)
        {
            Print("Credits Icesythe7, Zonz, PreciousTrainer");
        }

        //Generate the script
        private void CreateScript_Click(object sender, RoutedEventArgs e)
        {
            Print("Creating your custom script, please wait...");

            _codeBuilder = new StringBuilder();

            // Build path list
            if (_maps.Count > 1)
            {
                _codeBuilder.Append("local path = { ");
                foreach (string map in _maps)
                {
                    _codeBuilder.Append("'" + map + "', ");
                }
                WriteCode("}");
                WriteCode("");
            }

            //for those users who wants to use false swipe
            if (FalseSwipeCheckBox.IsChecked == true)
            {
                WriteCode("local falseSwipePokeUser = nil");
                WriteCode("");
            }

            WriteCode("local catchList =");
            WriteCode("{");
            foreach (var name in _names)
            {
                if (name.Contains(" ")) // Mr Mime / Farfetch 'd
                    WriteCode("['" + name + "'] = true,", 1);
                else
                    WriteCode(name + " = true,", 1);
            }
            WriteCode("}");

            WriteCode("");

            //onPathAction()
            if (_maps.Count > 1)
            {
                WriteCode("function onPathAction()");

                if (FalseSwipeCheckBox.IsChecked == true)
                    WriteCode("if isPokemonUsable(1) and getRemainingPowerPoints(falseSwipePokeUser, 'False Swipe') > 1 then", 1);
                else
                    WriteCode("if isPokemonUsable(1) then", 1);

                WriteCode("if lastMap() then", 2);
                WriteCode("return moveToGrass() or moveToWater() or moveToNormalGround()", 3);
                WriteCode("end", 2);
                WriteCode("elseif firstMap() then", 1);
                WriteCode("return usePokecenter()", 2);
                WriteCode("end", 1);
                WriteCode("end");
            }
            else
            {
                WriteCode("function onPathAction()");
                WriteCode("return moveToGrass() or moveToWater() or moveToNormalGround()", 1);
                WriteCode("end");
            }

            WriteCode("");

            //onBattleAction()
            WriteCode("function onBattleAction()");
            WriteCode("if isWildBattle() and (isOpponentShiny() or getOpponentForm() ~= 0 or catchList[getOpponentName()]) then", 1);
            
            if (FalseSwipeCheckBox.IsChecked == true)
            {
                WriteCode("if sendPokemon(falseSwipePokeUser) or (getOpponentHealthPercent() > 30 and useMove('False Swipe')) then", 2);
                WriteCode("return", 3);
                WriteCode("end", 2);
            }

            WriteCode("return useItem('Pokeball') or useItem('Great Ball') or useItem('Ultra Ball') or sendAnyPokemon()", 2);
            WriteCode("end", 1);
            if (AttackWildsCheckBox.IsChecked == true)
                WriteCode("return attack() or sendUsablePokemon() or logout()", 1);
            else
                WriteCode("return run() or attack() or sendAnyPokemon() or logout()", 1);
            WriteCode("end");

            //Return pokemon with false swipe
            if (FalseSwipeCheckBox.IsChecked == true)
            {
                WriteCode("");

                WriteCode("function onStart()");
                WriteCode("falseSwipePokeUser = getPokemonWithMove('False Swipe')", 1);
                WriteCode("end");

                WriteCode("");

                WriteCode("function getPokemonWithMove(moveName)");
                WriteCode("for i = 1, getTeamSize() do", 1);
                WriteCode("if hasMove(i, moveName) then", 2);
                WriteCode("return i", 3);
                WriteCode("end", 2);
                WriteCode("end", 1);
                WriteCode("fatal('No False Swiper could be found')", 1);
                WriteCode("return nil", 1);
                WriteCode("end");
            }

            if (_maps.Count > 1)
            {
                WriteCode("");

                WriteCode("local function firstMap()");
                WriteCode("if getMapName() == path[1] then", 1);
                WriteCode("return true", 2);
                WriteCode("end", 1);
                WriteCode("moveToMap(path[getPathIndex() - 1])", 1);
                WriteCode("end");

                WriteCode("");

                WriteCode("local function lastMap()");
                WriteCode("if getMapName() == path[#path] then", 1);
                WriteCode("return true", 2);
                WriteCode("end", 1);
                WriteCode("moveToMap(path[getPathIndex() + 1])", 1);
                WriteCode("end");

                WriteCode("");

                WriteCode("local function getPathIndex()");
                WriteCode("for i = 1, #path do", 1);
                WriteCode("if getMapName() == path[i] then", 2);
                WriteCode("return i", 3);
                WriteCode("end", 2);
                WriteCode("end", 1);
                WriteCode("fatal('error: current map is not present in the path table.')", 1);
                WriteCode("end");
            }

            //Copying the script
            System.Windows.Forms.Clipboard.SetText(_codeBuilder.ToString());

            Print("Script generated and copied to your clipboard.");
            

        }

        private void RegionBox_DropDownClosed(object sender, EventArgs e)
        {
            KantoMapsBox.Visibility = Visibility.Hidden;
            SeviiMapsBox.Visibility = Visibility.Hidden;
            JohtoMapsBox.Visibility = Visibility.Hidden;
            HoennMapsBox.Visibility = Visibility.Hidden;
            SinnohMapsBox.Visibility = Visibility.Hidden;
            EventMapsBox.Visibility = Visibility.Hidden;

            if (RegionBox.Text == "Kanto")
                KantoMapsBox.Visibility = Visibility.Visible;

            if (RegionBox.Text == "Sevii Islands")
                SeviiMapsBox.Visibility = Visibility.Visible;

            if (RegionBox.Text == "Johto")
                JohtoMapsBox.Visibility = Visibility.Visible;

            if (RegionBox.Text == "Hoenn")
                HoennMapsBox.Visibility = Visibility.Visible;

            if (RegionBox.Text == "Sinnoh")
                SinnohMapsBox.Visibility = Visibility.Visible;

            if (RegionBox.Text == "Event Maps")
                EventMapsBox.Visibility = Visibility.Visible;
        }
    }
}
